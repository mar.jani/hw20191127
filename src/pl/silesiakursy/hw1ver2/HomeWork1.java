package pl.silesiakursy.hw1ver2;

import java.util.Random;
import java.util.Scanner;

public class HomeWork1 {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        int number = random.nextInt(11);

        System.out.println("Podaj liczbę całkowitą");
        int scannerNumber = scanner.nextInt();

        for (;number != scannerNumber;) {
            if (number > scannerNumber){
                System.out.println("Za mało");
            } else if (number< scannerNumber){
                System.out.println("Za dużo");
            }
            System.out.println("Podaj liczbę całkowitą");
            scannerNumber = scanner.nextInt();
        }
        System.out.println("Udało się!");
    }
}
