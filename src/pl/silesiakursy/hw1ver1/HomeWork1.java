package pl.silesiakursy.hw1ver1;

import java.util.Random;
import java.util.Scanner;

public class HomeWork1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = getRandomNumber();
        int scannerNumber;

        do {
            System.out.println("Podaj liczbę całkowitą");
            scannerNumber = scanner.nextInt();

            if (equalsNumbers(number, scannerNumber) > 0){
                System.out.println("Za mało");
            } else if (equalsNumbers(number, scannerNumber) < 0){
                System.out.println("Za dużo");
            }else{
                System.out.println("Udało się!");
            }
        }while (equalsNumbers(number, scannerNumber) !=0);
    }

    private static int getRandomNumber() {
        Random random = new Random();
        return random.nextInt(11);
    }

    private static Integer equalsNumbers(Integer number, Integer scannerNumber) {
        return (number.compareTo(scannerNumber));
    }
}
