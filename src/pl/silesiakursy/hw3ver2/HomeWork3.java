package pl.silesiakursy.hw3ver2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class HomeWork3 {
    public static void main(String[] args) {
        List<Character> characterList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj trzy dowolne znaki:");
        String string = scanner.nextLine();
        for (int i = 0; i < 3 ; i++) {
            characterList.add(string.charAt(i));
        }
        Collections.sort(characterList);
        System.out.println(characterList);
    }
}
