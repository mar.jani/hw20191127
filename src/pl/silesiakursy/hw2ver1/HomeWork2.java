package pl.silesiakursy.hw2ver1;

import java.util.Scanner;

public class HomeWork2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę całkowitą");
        int number = scanner.nextInt();

        if (checkEven(number)) {
            System.out.println(" Podałeś liczbę parzystą!");
        } else {
            System.out.println("Podałeś liczbę nieparzystą!");
        }
    }

    public static boolean checkEven(int number) {
        return (number % 2 == 0);
    }
}
