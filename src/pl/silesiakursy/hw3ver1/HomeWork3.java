package pl.silesiakursy.hw3ver1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class HomeWork3 {

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            stringList.add(getString());
        }

        Collections.sort(stringList);

        for (String s : stringList) {
            System.out.println(s);
        }
    }

    private static String getString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dowolny znak");
        return String.valueOf(scanner.next().charAt(0));
    }
}
